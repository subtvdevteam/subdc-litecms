/*

Subdc Lite Webserver.

Huge amount of scope creep here.  Needs a good refactor once its live.

Kept all in one file for development, needs separating into modules ideally.

Hosted in Azure, using a shared instance,  will need promoting to a reserved instance if sales take off.

S3 for storage -- the way this is being done at the moment isnt hugely efficient - but it was quicker to do it this way.

Plupload and transload it do things in different wasys that dont compliment each other

//

Files are uploaded to Transloadit.  THey get processed and the within the assembley instructions the get moved to a cache folder subdc/:accountid/Cache
Once the transloadit service is complete it posts a very large JSON object back to this API, which contains the unique filesname and other meta data.
Then the Transloadit method in here moves the files over the subdc/media/:channelID/:mediatype/:uniquefilename.
Its quite elegant BUT because its on distributed systems tracking is difficult.

*/
///Database Set Up.  Using Amazon. RDS (Ireland)
var host_name = 'subdc.c83zg2uetiht.eu-west-1.rds.amazonaws.com';
var user_name = 'subdcroot';
var db_pwd = '0308Pr0sper';
var db_schema = 'subdc_schema';
var db = require('mysql');

var pool = db.createPool({
    host: host_name,
    user: user_name,
    password: db_pwd,
    database: db_schema,
    connectionLimit: 100,
    stringifyObjects: true
})
// Include Express as Framework.  -- Bluesky is the Azure File Base DB...
var express = require('express'),
    BlueskyStore = require('connect-bluesky')(express);
//Include Amazon S3.  Use the SubDC Bucket
require('awssum');
var amazonS3 = require('awssum-amazon-s3');
var s3 = new amazonS3.S3({
    'accessKeyId': '1T4SW86N1G5CSNNBC482',
    'secretAccessKey': '3Cg58tbxHMce0or7QcqQFabGa60IohgYXH7KjrW1',
    'region': amazonS3.EU_WEST_1

});

var users = {}
var user;
//Using a connection pool as RDS on Amazon has low connection limits. The default pool is 10, this can be increased.  If more connections are required then this would also have to be reflected by the Server Size on Amazon.
//Here we load all the users and put them into a JSON array.
pool.getConnection(function(err, connection) {
    connection.query('SELECT users_usr.user_name_usr AS username, users_usr.password_usr AS `password`, users_usr.idact_usr AS acctID,  users_usr.id_usr AS userID,  acttoclg_atc.idclg_atc AS cnlgrp,  channel_folders_cnf.id_cnf AS folderID FROM users_usr INNER JOIN acttoclg_atc ON users_usr.idact_usr = acttoclg_atc.idact_atc INNER JOIN channel_folders_cnf ON users_usr.id_usr = channel_folders_cnf.idusr_cnf', function(error, rows, fields) {
        if (error) {
            console.log(error)
        }
        else {
            users = rows;
        }
        connection.end();
    });
});
//Basic set up of the Express App.
var app = express();
app.configure(function() {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade'); // Not using this....
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser(''));
    var store = new BlueskyStore({
        account: 'bssub',
        key: 'pMRphnTnVCgi/Gr9721lOU9Fl4Jbq03Vhcb0p5ZBJ+5IMfxwo7UyQaV6Q6JXf2OGIz22mewYzBz75z26lwepoA==',
        table: 'sessions'
    })
    app.use(express.session({

        secret: 'HC SVNT DRACONES',
        cookie: {
            maxAge: 90000000 // This is INSANE but this was specificly requested by a client.
        },
        store: store
    }));

    app.use(express.static(__dirname));
});
/////////////////////////////////////////////////////////////
///////////////////////  Web Server /////////////////////////
////////////////////////////////////////////////////////////

//Esentially these are just routes.
app.get('/', requiresLogin, function(req, res) {

    res.sendfile('Dash.html');
});
app.get('/logout', function(req, res) {
    req.session.destroy();

    res.redirect('/login');
});
app.get('/login', function(req, res) {

    res.sendfile('LogIn.html');

});
app.get('/contentmanager', requiresLogin, function(req, res) {

    res.sendfile('ContentManager.html');
});

app.get('/addchannel', requiresLogin, function(req, res) {


    res.sendfile('AddDroid.html');
});
//The post for log in..
app.post('/login', function(req, res) {

    authenticate(req.body.username, req.body.password, function(user) {
        if (user) {
            req.session.user = user;
            res.sendfile('Dash.html');
        }
        else {
            res.redirect('/login');
        }
    });


});
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// REST SERVICE - COULD BE EXPOSED AS AN API ///////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

/// GET -- Channel Folders.
/// Each Channel is associated with a folder, this will allow for grouping of Channels at a later date.
/// At the moment the UI does not allow for the creation of folders, when a channel is created it gets put into a "Everything" folder.
app.get('/channelfolders/:act', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('SELECT channel_folders_cnf.name_cnf, channel_folders_cnf.id_cnf, channel_folders_cnf.idusr_cnf FROM channel_folders_cnf where idusr_cnf = ' + req.session.user.userID, function(err, rows, fields) {
            if (err) {

                console.log("ERROR" + err);
            }
            res.end(req.query.callback + '(' + JSON.stringify(rows) + ')');
        });
        connection.end();
    });
});
/// THIS IS A PAIN  But because we arent using Jade we cant send Server variables to the client, so they have to be requested...
/// Basically this is just to fill in the Username on the top Right of the UI
app.get('/getusername', function(req, res) {

    res.end(req.session.user.username);
});
/// REFACTOR  THis actually returns the ACCOUNT ID not the ChannelID --
/// Needs renaming GetAccountID, but then also needs all ajax calls to be updated.
app.get('/getChannelID', function(req, res) {
    res.end('' + req.session.user.acctID);
});
/// Simple Query to get all the channels in the Channel Group connected to the User in the session.
app.get('/getchannels/', function(req, res) {
    pool.getConnection(function(err, connection) {
        var query = connection.query('select id_cnl, name_cnl,sixdigitcode_cnl,is_portrait_cnl, DATE_FORMAT(date_created_cnl,"%D %M %Y") as date_created from channels_cnl where idclg_cnl =' + req.session.user.cnlgrp, function(err, rows, fields) {
            if (err) {

                console.log("ERROR" + err);
            }
            res.end(req.query.callback + '(' + JSON.stringify(rows) + ')');
        });
        connection.end();
    });
});
///  Intention here is to enable content to be categorised as per the UI design from Neeri Reddy.
///  However this might be deprecated in favour of tagging...
app.get('/ccg/:act', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('SELECT content_categories_ccg.id_ccg, content_categories_ccg.name_ccg, content_categories_ccg.act_ccg FROM content_categories_ccg where act_ccg=' + req.session.user.acctID, function(error, rows, fields) {
            if (error) {
                res.setHeader('Content-Type', 'application/json');
                res.send(error);
            }
            res.end(req.query.callback + '(' + JSON.stringify(rows) + ')');
        });
        connection.end();
    });
});

/// Returns only non-expired media for the given channel ID -- Used exclusively for the Content re-ordering Model.
app.get('/validmedia/:channel', function(req, res) {

    pool.getConnection(function(err, connection) {

        /**
         * Returns valid media only in the correct order from the media_schedule_mds table
         */
        connection.query('SELECT  media_mda.id_mda, media_mda.filename_mda, ' + 'media_mda.format_mda, media_mda.date_start_mda, media_mda.date_end_mda, ' + 'media_mda.duration_mda, media_mda.media_thumbnail_mda, media_mda.file_title_mda, ' + 'media_mda.idcnl_mda ' + 'FROM media_mda ' + 'LEFT OUTER JOIN media_schedule_mds ON media_mda.id_mda = media_schedule_mds.idmda_mds ' + 'WHERE media_mda.is_data_deleted_mda = 0 ' + 'AND DATE(media_mda.date_start_mda) <= DATE(now()) ' + 'AND DATE(media_mda.date_end_mda) >= DATE(now()) ' + 'AND media_mda.idcnl_mda = ' + req.params.channel + ' ORDER BY media_schedule_mds.position_mds ASC, id_mda ASC ', function(error, rows, fields) {
            res.setHeader('Content-Type', 'application/json');
            if (error) {
                res.send(error);
            }
            res.json(rows);
        });

        connection.end();
    });
});

/// Saves the media schedule for the given channelID
/// Parameters:  :channel - Channel ID
/// :schedule - Comma-delimited list of Media ID's in the desired order
app.get('/saveMediaSchedule/:channel/:schedule', function(req, res) {

    if (req.params.channel > 0 && req.params.schedule !== "") {

        var connectionPool = db.createConnection({
            host: host_name,
            user: user_name,
            password: db_pwd,
            database: db_schema
        });
        connectionPool.connect();
        var mediaIDAr = req.params.schedule.split(',');
        if (mediaIDAr.length > 0) {
            // Delete the media schedule for the given channel ID before persisting the new one
            var removeScheduleSQL = "DELETE FROM media_schedule_mds WHERE idcnl_mds = " + req.params.channel;
            connectionPool.query(removeScheduleSQL, function(err, results, fields) {
                if (err) {
                    console.log(err);
                    EmailError(err);
                }
                else { // successfully deleted the old schedule, now persist the new one
                    for (var i = 0; i < mediaIDAr.length; i++) {
                        // persist the new schedule afte we've successfully deleted the old one
                        var persistScheduleSQL = "INSERT INTO media_schedule_mds (idmda_mds, position_mds, idcnl_mds,is_data_in_sync_mds) " + "VALUES (" + mediaIDAr[i] + ", " + i + ", " + req.params.channel + ", 0) ";
                        connectionPool.query(persistScheduleSQL, function(err, results, fields) {

                            if (err) {
                                console.log(err);
                                EmailError(err);
                            }

                            console.log("r" + JSON.stringify(results));

                        });
                    }
                }
                console.log("r" + JSON.stringify(results));
                res.setHeader('Content-Type', 'application/json');
                res.end("OK");
                connectionPool.end();
            });
        }
    }
});
/// Gets One Media Item where :fileid = the id_mda.
app.get('/getMediaItem/:fileid', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('select id_mda,idcnl_mda,media_thumbnail_mda, filename_mda, date_created_mda,date_end_mda,date_start_mda from media_mda where  id_mda =' + req.params.fileid, function(error, rows, fields) {
            res.setHeader('Content-Type', 'application/json');
            if (error) {
                res.send(error);
            }
            res.send(req.query.callback + '(' + JSON.stringify(rows) + ')');
        });
        connection.end();
    });
});

/////////////////////////DEPRICATED  Now using the table to filter all results
app.get('/media/:channel/:live', function(req, res) {
    pool.getConnection(function(err, connection) {


        connection.query('SELECT channels_cnl.name_cnl,  media_mda.id_mda,media_mda.filename_mda,media_mda.format_mda,media_mda.date_start_mda,media_mda.date_end_mda,media_mda.duration_mda,media_mda.media_thumbnail_mda,media_mda.file_title_mda,media_mda.idcnl_mda,CAST(media_mda.is_morning_mda as UNSIGNED int) as is_morning_mda,CAST(media_mda.is_afternoon_mda as UNSIGNED int) as is_afternoon_mda,CAST(media_mda.is_evening_mda as UNSIGNED int) as is_evening_mda FROM media_mda INNER JOIN channels_cnl ON media_mda.idcnl_mda = channels_cnl.id_cnl WHERE media_mda.is_data_deleted_mda = 0   and channels_cnl.idclg_cnl = ' + req.session.user.cnlgrp + ' ORDER BY media_mda.id_mda DESC  ', function(error, rows, fields) {
            console.log('SELECT channels_cnl.name_cnl,  media_mda.id_mda,media_mda.filename_mda,media_mda.format_mda,media_mda.date_start_mda,media_mda.date_end_mda,media_mda.duration_mda,media_mda.media_thumbnail_mda,media_mda.file_title_mda,media_mda.idcnl_mda,CAST(media_mda.is_morning_mda as UNSIGNED int) as is_morning_mda,CAST(media_mda.is_afternoon_mda as UNSIGNED int) as is_afternoon_mda,CAST(media_mda.is_evening_mda as UNSIGNED int) as is_evening_mda FROM media_mda INNER JOIN channels_cnl ON media_mda.idcnl_mda = channels_cnl.id_cnl WHERE media_mda.is_data_deleted_mda = 0 and channels_cnl.idclg_cnl = ' + req.session.user.cnlgrp + ' ORDER BY media_mda.id_mda DESC');
            res.setHeader('Content-Type', 'application/json');
            if (error) {
                res.send(error);
            }

            res.json(rows);
            //res.end(rows)

        });

        connection.end();
    });

});
/// Change the Orientation of a player.
app.get('/changeOrientation/:orientation/:channel', function(req, res) {
    var queryString;
    if (req.params.orientation == 'Portrait') {
        queryString = "update channels_cnl set is_portrait_cnl = 1 where id_cnl = " + req.params.channel;
    }
    else {
        queryString = "update channels_cnl set is_portrait_cnl = 0 where id_cnl = " + req.params.channel;
    }

    var connection = db.createConnection({
        host: host_name,
        user: user_name,
        password: db_pwd,
        database: db_schema,
        stringifyObjects: true
    });
    connection.connect();
    connection.query(queryString, function(err, rows, fields) {


    });
    connect.destroy();
    res.end();
});
/// Simple Query to get all the channels in the Channel Group connected to the User in the session.
app.get('/getchannels/', function(req, res) {
    pool.getConnection(function(err, connection) {
        var query = connection.query('select id_cnl, name_cnl,sixdigitcode_cnl,is_portrait_cnl, DATE_FORMAT(date_created_cnl,"%D %M %Y") as date_created from channels_cnl where idclg_cnl =' + req.session.user.cnlgrp, function(err, rows, fields) {
            if (err) {

             
            }
            res.end(req.query.callback + '(' + JSON.stringify(rows) + ')');
        });
        connection.end();
    });
});
/// Get all the channels for a specific folder.
app.get('/channels/:folderID', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('SELECT channel_to_folder_ctf.id_ctf, channel_to_folder_ctf.idcnl_ctf, channel_to_folder_ctf.idcnf_ctf, channels_cnl.name_cnl FROM channel_to_folder_ctf INNER JOIN channels_cnl ON channel_to_folder_ctf.idcnl_ctf = channels_cnl.id_cnl WHERE channel_to_folder_ctf.idcnf_ctf =' + req.params.folderID, function(err, rows, fields) {
            if (err) {

                console.log("ERROR" + err);
            }
            res.send(req.query.callback + '(' + JSON.stringify(rows) + ')');
        });
        connection.end();
    });
});
/// Get all tags for a specific channel.
app.get('/getTags/:channelID',function(req,res) {
    pool.getConnection(function(err, connection) {
        connection.query('SELECT tags_tag.name_tag FROM tagtocnl_tgc INNER JOIN tags_tag ON tagtocnl_tgc.idtag_tgc = tags_tag.id_tag WHERE idcnl_tgc = '+req.params.channelID, function(err,results,fields){
            if(err){}
                tagArray = [];
                for(var result = 0; result < results.length; result ++)
                {
                    tagArray.push(results[result].name_tag);
                    console.log(results[result].name_tag);
                }
            res.end(''+tagArray);

        });
        connection.end();
        
    })
});
//Get all channels for a specific tag.
app.get('/taggedChannels/:tag',function(req,res){
pool.getConnection(function(err,connection){
connection.query('SELECT channels_cnl.orientation_cnl, channels_cnl.name_cnl, channels_cnl.id_cnl, channels_cnl.idclg_cnl FROM tagtocnl_tgc INNER JOIN channels_cnl ON tagtocnl_tgc.idcnl_tgc = channels_cnl.id_cnl INNER JOIN tags_tag ON tags_tag.id_tag = tagtocnl_tgc.idtag_tgc WHERE name_tag = "'+req.params.tag+'" AND idclg_cnl = '+req.session.user.cnlgrp, function(err,results){
console.log(results);
var returnString =[];
for(var result = 0; result < results.length; result ++)
{
    var channelObj = {"channelID" : results[result].id_cnl , "name" : results[result].id_cnl};

returnString.push(channelObj);
}

res.jsonp(200,returnString);

});
connection.end();
});

});
app.get('/saveTags/:tags/:channelID', function(req,res){
    pool.getConnection(function(err,connection){
        connection.query('Delete From tagtocnl_tgc where idcnl_tgc ='+req.params.channelID,function(err,res,field){})
        InsertChannelTags(req.params.channelID, req.params.tags);
    })
})
//Get All Tags for Media
app.get('/getMediaTags/:mediaID',function(req,res){
pool.getConnection(function(err,connection){
    connection.query('SELECT tags_tag.name_tag FROM tagtomda_tgm INNER JOIN tags_tag ON tagtomda_tgm.idtag_tgm = tags_tag.id_tag WHERE idmda_tgm = '+req.params.mediaID,function(err,results){
        res.json(results);
    })
    connection.end();
});
res.end();
});
//// POST ////////

//  REFACTOR  CALL updateChannelName as this POST updates the Name of the Channel.
app.post('/updatechannel', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('UPDATE channels_cnl SET name_cnl = "' + req.body.value + '" WHERE id_cnl = ' + req.body.pk, function(err, results, fields) {

            if (err) {

            }
        });
        connection.end();
    });
    res.end(req.body.pk);

});
/// REFACTOR CALL UpdateMediaTitle as this POST updates the Title of a piece of media
/// (by default this is the original filename)
app.post('/updatetitle', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('UPDATE media_mda SET file_title_mda = "' + req.body.value + '" WHERE id_mda = ' + req.body.pk, function(err, results, fields) {

            if (err) {

            }
        });
        connection.end();
    });
    res.end(req.body.pk);
});
/// When the file upload handler from Plupload executed this POST gets called.  In testing there were a few issues with saving data.
/// THis was due to max connections on the MySQL database and not the code..
app.post('/fileuploadcomplete', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    var resp = 'Length' + req.body.uploadedFile.channels.length;
    //var categories = req.body.uploadedFile.categories;
    var channels = req.body.uploadedFile.channels;
    var morning = req.body.uploadedFile.morning;
    var afternoon = req.body.uploadedFile.afternoon;
    var evening = req.body.uploadedFile.evening;
    var startDate = req.body.uploadedFile.startDate;
    console.log("fileuploadcomplete:"+startDate);
    var endDate = req.body.uploadedFile.endDate;
    var advScheduling = req.body.uploadedFile.advancedSchedule;
   console.log('Channels:'+channels);
    var duration = req.body.uploadedFile.duration; // If this is a video the Transloadit response will overwrite the duration, based on the extracted metadata
    if (channels.length > 0) {
        for (var i = 0; i < channels.length; i++) {
            var cid = channels[i].channelID;
            InsertNew(cid, req, morning, afternoon, evening, startDate, endDate, duration, advScheduling); /// refactor to call "InsertNewMedia"
        }
    }
    res.end("length" + channels.length);
});
//Executes when all are finished - added to avoid a 404 but also to allow for additional feedback...
app.post('/contentUploaded', function(req, res) {
    res.end();
});
//////GROAN :( This gets called by the transloadit service once the media has been processed.  Some of the data sent back is inconsistant.
//////When there is more time this need lookig at carefully.
//////Its a big old method,  it really needs separating out a bit, but no time...
app.post('/transloadit/', function(req, res) {
    var errorMessage = '';
    var channelsArray = [];
    var transloadit = JSON.parse(req.body.transloadit); // Because all of the transloadit data is in one key "transloadit"
    var accountID = transloadit.fields.accountID; // This is sent by Plupload for everyfile

    if (transloadit.uploads[0].type == 'video') {

        var thumbnailFile = transloadit.fields.name.substring(0, transloadit.fields.name.length - 4);
        var thumbnail = thumbnailFile + '.' + transloadit.results.extracted_thumbs[0].ext;
        var duration = transloadit.uploads[0].meta.duration;
        var format = 'VIDEO';
        //get every channel for the account
        pool.getConnection(function(err, connection) {

            connection.query('SELECT channels_cnl.id_cnl, channels_cnl.name_cnl,  acttoclg_atc.idact_atc FROM acttoclg_atc INNER JOIN channels_cnl ON acttoclg_atc.idclg_atc = channels_cnl.idclg_cnl WHERE idact_atc =' + accountID, function(err, results, fields) {
                if (err) {
                    console.log(err);
                    EmailError(err);
                }

                if (results.length > 0) {
                    //We have results so put them into an Array,  this might seem unnecessary, but in testing using the JSON directly had some issues.

                    for (var i = 0; i < results.length; i++) {
                        var channelObj = {
                            channelID: results[i].id_cnl,
                            channelOrientation: results[i].is_portrait_cnl
                        };
                        channelsArray.push(channelObj);

                    }

                    //Loop through the newly created array.
                    for (var i = 0; i < channelsArray.length; i++) {

                        //actual File
                        var filenameR = transloadit.results["android_video"][0].name.replace(/[(&)]/g, '');
                        //Make a unique name to avoid overwriting another client copy.
                        var uniqueName = transloadit.fields.name.replace(/\.[^\.]*$/, '.mp4')
                        /// Amazon calls the destination the key -- Hence I am keeping the same terminology
                        var key = 'Media/' + channelsArray[i].channelID + '/video/' + uniqueName.replace(/ /gi, '-');
                        //Check to see if it is a portrait channel.
                        // The template created on Transloadit prefixes 'rotated_video' if the file is rotated
                        if (channelsArray[i].channelOrientation == 1) {

                            var copySource = 'Media/' + accountID + '/cache/rotated_video-' + filenameR.replace(/ /gi, '-');
                        }
                        else {
                            var copySource = 'Media/' + accountID + '/cache/android_video-' + filenameR.replace(/ /gi, '-');
                        }

                        s3CopyObject(key, copySource); // This moves the files from the /cache directory to the /Video directory for each channel

                        //thumbNail
                        var filenameR = transloadit.results[":original"][0].name.replace(/[(&)]/g, '');
                        var uniqueName = transloadit.fields.name.replace(/[(&)]/g, '');
                        var key = 'Media/' + channelsArray[i].channelID + '/video/' + uniqueName.replace(/ /gi, '-').substring(0, uniqueName.length - 3) + transloadit.results.extracted_thumbs[0].ext;
                        var copySource = 'Media/' + accountID + '/cache/extracted_thumbs-' + filenameR.replace(/ /gi, '-').substring(0, filenameR.replace(/ /gi, '-').length - 3) + transloadit.results.extracted_thumbs[0].ext;

                        s3CopyObject(key, copySource); // Does the same but for the Thumbnails.

                        var up_id = transloadit.fields.name.replace(/[()]/g, '').substring(0, (transloadit.fields.name.replace(/[()&]/g, '').length - 4));
                        var filenameR = transloadit.results[":original"][0].name.replace(/[(&)]/g, '');
                        //NOW UPDATE THE RECORDS

                        var preUrl = 'https://s3-eu-west-1.amazonaws.com/subdc/Media/' + channelsArray[i].channelID + '/video/' + thumbnail;
                        var queryString = 'UPDATE media_mda set filename_mda = "' + uniqueName.replace(/ /gi, '-') + '",format_mda = "VIDEO", duration_mda=' + duration + ',is_data_in_sync_mda =0, media_thumbnail_mda = "' + preUrl + '" WHERE upload_id_mda =  "' + up_id + '" AND idcnl_mda =' + channelsArray[i].channelID;

                        UpdateRow(queryString); // Update the Media so it is now set for the player to download, the thumbnail is added, the file is renamed to the unique filename and the duration updated to reflect the length of the video
                    }
                }
            });
            connection.end();
        });

    }
    else {
        ///////////// UPLOAD WAS AN IMAGE ///////////////

        var thumbnailFile = transloadit.results[':original'][0].name;
        pool.getConnection(function(err, connection) {
            if (err) {
                console.log(err)
            };
            var format = 'IMAGE';
            console.log('SELECT channels_cnl.id_cnl, channels_cnl.name_cnl,channels_cnl.is_portrait_cnl,  acttoclg_atc.idact_atc FROM acttoclg_atc INNER JOIN channels_cnl ON acttoclg_atc.idclg_atc = channels_cnl.idclg_cnl WHERE idclg_cnl =  ' + accountID);
            connection.query('SELECT channels_cnl.id_cnl, channels_cnl.name_cnl,channels_cnl.is_portrait_cnl,  acttoclg_atc.idact_atc FROM acttoclg_atc INNER JOIN channels_cnl ON acttoclg_atc.idclg_atc = channels_cnl.idclg_cnl WHERE idact_atc =  ' + accountID, function(err, results, fields) {
                if (err) {
                    console.log(err);
                    errorMessage += err;
                    EmailError(err);
                }
                ////Same as we did for videos.
                if (results.length > 0) {


                    for (var i = 0; i < results.length; i++) {
                        console.log("db orientation: " + results[i].is_portrait_cnl);
                        var channelObj = {
                            channelID: results[i].id_cnl,
                            channelOrientation: results[i].is_portrait_cnl
                        };
                        channelsArray.push(channelObj);

                    }


                    for (var i = 0; i < channelsArray.length; i++) {
                        var copySource;
                        var filenameR = transloadit.results[":original"][0].name.replace(/[()]/g, '');
                        var uniqueName = transloadit.fields.name.replace(/[()]/g, '');
                        var key = 'Media/' + channelsArray[i].channelID + '/image/' + uniqueName.replace(/ /gi, '-');

                        if (channelsArray[i].channelOrientation == 1) {
                            copySource = 'Media/' + accountID + '/cache/rotated_image-' + filenameR.replace(/ /gi, '-');
                        }
                        else {
                            copySource = 'Media/' + accountID + '/cache/:original-' + filenameR.replace(/ /gi, '-');
                        }
                        console.log("moving: " + copySource + " => " + key);
                        s3CopyObject(key, copySource);
                        var preUrl = transloadit.results["image_thumb"][0].url;
                        var up_id = transloadit.fields.name.replace(/[()]/g, '').substring(0, (transloadit.fields.name.replace(/[()]/g, '').length - 4));
                        var queryStringForImages = 'UPDATE media_mda set filename_mda = "' + transloadit.fields.name.replace(/[()]/g, '') + '", format_mda = "IMAGE",is_data_in_sync_mda =0, media_thumbnail_mda = "' + preUrl + '" WHERE upload_id_mda = "' + up_id + '"AND idcnl_mda =' + channelsArray[i].channelID;

                        UpdateRow(queryStringForImages);
                    }

                }
                connection.end();
            });

        });

    }
    res.end("error:");
});

////Baljit's method.  It needs renaming because its actually updates rather than uploads.... Does the job though
app.post('/modalupload', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin');
    res.writeHead(200);
    var reqMorning = req.body.updateData.morning;
    var reqAfternoon = req.body.updateData.afternoon;
    var reqEvening = req.body.updateData.evening;
    var startDate = req.body.updateData.startDate;
    var endDate = req.body.updateData.endDate;
    var cid = req.body.updateData.fileID;

    var morning = parseInt(reqMorning);
    var afternoon = parseInt(reqAfternoon);
    var evening = parseInt(reqEvening);

    updateFromModal(morning, afternoon, evening, startDate, endDate, cid);

    res.end('{}');
});

/// This just deletes a row from the database.  NB nothing is ever deleted the is_deleted is merely made a 1.
app.post('/deleterow', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin');
    res.writeHead(200);
    var cid = req.body.mediaID;
    deleteMedia(cid);
    res.end('{}');
});
////Baljits method.  THis is a bit concerning,  needs making safer I think // Refactor when Live.
app.post('/massDelete', function(req, res) {
    var cid = req.body.mediaID;
    var x = 0; // refactor x is useless
    while (x !== cid.length) {

        var mid = cid[x];
        pool.getConnection(function(err, connection) {
            connection.query('UPDATE media_mda SET is_data_deleted_mda = 1, is_data_in_sync_mda = 0 WHERE id_mda = ' + mid + '', function(err, results, fields) {
                if (err) {
                    res.send("error:" + err)
                }

            });

            x++;
            connection.end();
        });
    }

    res.end('{}');
});
///Baljits method.  Updates media en mass.  Works well.
app.post('/massUpdate', function(req, res) {

    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin');
    res.writeHead(200);

    var filesAr = req.body.mediaID.files;

    var morning = req.body.mediaID.morning;
    var afternoon = req.body.mediaID.afternoon;
    var evening = req.body.mediaID.evening;

    var startDate = req.body.mediaID.startDate;
    var endDate = req.body.mediaID.endDate;


    //    console.log('cid length:'+cid.length);
    //    console.log('cid:'+cid);

    var x = 0; // refactor no x's please

    while (x < filesAr.length) {

        var fileID = filesAr[x];

        updateFromModal(morning, afternoon, evening, startDate, endDate, fileID);

        x++;
    }

    res.end('{}');
});
/// Register..  Another long one.  This is because so much has to happen when someone registers..
app.post('/register', function(req, res) {
    var u;
    var cnlGRP;
    pool.getConnection(function(err, connection) {
        connection.query('select id_usr from users_usr where user_name_usr = "' + req.body.username + '" and password_usr = "' + req.body.password + '"', function(err, rows, fields) {

            if (err) {
                console.log(err);
            }

            if (rows.length > 0) {
                res.write('exists'); // Basically this combo already exists...
                res.end();
            }
            else {

                // No records so this is a new user.
                res.write('new user');

                var actID = 0;
                /// 1) Firstly Create an Account based on the company name to hold the user.
                connection.query('insert into accounts_act SET ?', {
                    name_act: req.body.company
                }, function(err, result) {
                    actID = result.insertId;

                });
                /// 2) Then add a channel_group to hold the channels for the account created above.
                connection.query('insert into subdc_schema.channel_groups_clg SET ?', {
                    name_clg: req.body.company,
                    has_subfolders_clg: 0,
                    is_subfolder_clg: 0,
                    has_channel_clg: 0
                }, function(err, result) {


                    cnlGRP = result.insertId;
                    // 3) Associate the Channel Group to the Account as processed in 1 & 2 above
                    insertIntoAct2CLG(actID, result.insertId);
                    // 4) Finally add the user and associate with the record created in 3 above.
                    connection.query('INSERT INTO users_usr SET ?', {
                        user_name_usr: req.body.username,
                        password_usr: req.body.password,
                        email_address_usr: req.body.email,
                        first_name_usr: req.body.firstname,
                        last_name_usr: req.body.lastname,
                        company_name_usr: req.body.company,
                        is_admin_usr: 1,
                        idact_usr: actID
                    }, function(err, result) {


                        console.log(result.insertId);
                        u = {
                            username: req.body.username,
                            password: req.body.password,
                            acctID: actID,
                            userID: result.insertId,
                            cnlgrp: cnlGRP
                        };
                        // Update the users model.
                        update_users();
                        //Log the newly created user in
                        req.session.user = u;
                        // 5) Finally create a channel folder to put all the users channels in -- see note on channel folders within the GET method for channel folders...
                        connection.query('insert into channel_folders_cnf SET ?', {
                            name_cnf: 'All Channels',
                            idusr_cnf: result.insertId
                        }, function(err, result) {

                        });
                        res.end('ok');
                    });
                });
            }
        });
    });

});
/// To add a channel.
app.post('/addChannel', function(req, res) {
    var connection = db.createConnection({
        host: host_name,
        user: user_name,
        password: db_pwd,
        database: db_schema,
        stringifyObjects: true
    });
    connection.connect();
    var authcode = req.body.authcode;
    var licencekey = req.body.licence;
    var channelname = req.body.channelname;
    var userid = req.body.userid;
    var userclg = req.body.userclg;
    var tags = req.body.tags;
    var channelID;


    connection.query('INSERT into channels_cnl SET ?', {
        name_cnl: channelname,
        has_livetv_cnl: 0,
        sixdigitcode_cnl: authcode,
        idclg_cnl: req.session.user.cnlgrp
    }, function(err, result) {
        /// for some reason scope of vars added here is lost out of this function, hence Im am sending them within it
        res.send(result.insertId);
        InsertChannelToFolder(result.insertId, req.session.user.folderID); // adds the new channel to a folder
        InsertChannelTags(result.insertId, tags); // Tags the channel
    });

    connection.end();
});
// Mainly for IIS (company not webserver) This will allow for adding tags with a lookahead.  Currently not used.
app.post('/ajax/tags', function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query('SELECT tags_tag.name_tag FROM tagtocnl_tgc INNER JOIN tags_tag ON tagtocnl_tgc.idtag_tgc = tags_tag.id_tag INNER JOIN channels_cnl ON tagtocnl_tgc.idcnl_tgc = channels_cnl.id_cnl WHERE idclg_cnl =' + req.session.user.cnlgrp, function(err, result) {

            if (err) {
                console.log(err)
            }
            res.end(result)
        });
        connection.end();
    });
})
/////////////////////////////////////////////////////////////////////////
///////////////////////////////////HELPERS//////////////////////////////
////////////////////////////////////////////////////////////////////////

// Inserts a new piece of media into the database.
function InsertNew(query, req, morning, afternoon, evening, startDate, endDate, duration,advancedSchedule) {
    console.log("InsertNew: "+startDate);
    // Crappy JQuery Calendar Plug in substringing ......
    var mysqlStartYear = startDate.substr(6, 10);
    var mysqlStartMonth = startDate.substr(3, 2);
    var mysqlStartDay = startDate.substr(0, 2);

    var mysqlEndYear = endDate.substr(6, 10);
    var mysqlEndMonth = endDate.substr(3, 2);
    var mysqlEndDay = endDate.substr(0, 2);
console.log("query var = "+query);
    var start = mysqlStartYear + '-' + mysqlStartMonth + '-' + mysqlStartDay;

    var end = mysqlEndYear + '-' + mysqlEndMonth + '-' + mysqlEndDay;
    var filenameR = req.body.uploadedFile.fileName.replace(/[()]/g, '');
    var advS = 0;
    console.log(advancedSchedule);
    if(advancedSchedule.length > 0)
    {
        console.log('advS:1');
        advS = 1;
    }
   
        console.log("s: "+start);
            
            
                console.log('past error');
                if(advS == 0){
                ///actually does the insert.. the above prepares the query...
                var filenameR = req.body.uploadedFile.fileName.replace(/[()]/g, '');
                var queryString = 'INSERT into media_mda (filename_mda,file_title_mda,idcnl_mda,is_morning_mda,is_evening_mda,is_afternoon_mda,date_start_mda,date_end_mda,is_data_in_sync_mda,upload_id_mda,duration_mda,media_thumbnail_mda, has_advanced_schedule_mda) VALUES("' + filenameR.replace(/ /gi, '-') + '","' + filenameR.replace(/ /gi, '-') + '",' + query + ',' + morning + ',' + evening + ',' + afternoon + ',"' + start + '","' + end + '",1,"' + req.body.uploadedFile.fileID + '", ' + req.body.uploadedFile.duration + ',"https://s3-eu-west-1.amazonaws.com/subdc/Media/processing.png",'+advS+')';
               console.log('Basic: '+queryString);
                InsertNew2(queryString);
            }else{
              
                for(var itemToadd = 0; itemToadd < advancedSchedule.length; itemToadd ++)
                {
                    startDate = new Date(advancedSchedule[itemToadd]);
                    var filenameR = req.body.uploadedFile.fileName.replace(/[()]/g, '');
                    var start  = startDate.valueOf()/1000;
                     var end = advancedSchedule[itemToadd];
                    console.log('In Advanced Loop:' + start);
                    var queryString = 'INSERT into media_mda (filename_mda,file_title_mda,idcnl_mda,is_morning_mda,is_evening_mda,is_afternoon_mda,date_start_mda,date_end_mda,is_data_in_sync_mda,upload_id_mda,duration_mda,media_thumbnail_mda, has_advanced_schedule_mda) VALUES("' + filenameR.replace(/ /gi, '-') + '","' + filenameR.replace(/ /gi, '-') + '",' + query + ',' + morning + ',' + evening + ',' + afternoon + ', FROM_UNIXTIME("' + start + '"),FROM_UNIXTIME("' + start + '"),1,"' + req.body.uploadedFile.fileID + '", ' + req.body.uploadedFile.duration + ',"https://s3-eu-west-1.amazonaws.com/subdc/Media/processing.png",'+advS+')';
                   console.log('Adv: '+queryString);
                    InsertNew2(queryString);
                }
            }

           
       
    
}


function updateFromModal(morning, afternoon, evening, startDate, endDate, cid) {

    var mysqlStartYear = startDate.substr(6, 10);

    var mysqlStartMonth = startDate.substr(3, 2);
    var mysqlStartDay = startDate.substr(0, 2);

    var mysqlEndYear = endDate.substr(6, 10);
    var mysqlEndMonth = endDate.substr(3, 2);
    var mysqlEndDay = endDate.substr(0, 2);

    var start = mysqlStartYear + "-" + mysqlStartMonth + "-" + mysqlStartDay;

    var end = mysqlEndYear + "-" + mysqlEndMonth + "-" + mysqlEndDay;

    //    var queryC = connection.query('SELECT * from media_mda where filename_mda ="'+req.body.uploadedFile.fileName+'" AND idcnl_mda ='+query,function(err, results, fields){

    var queryString = ' UPDATE media_mda SET is_morning_mda = ' + morning + ',is_evening_mda = ' + evening + ',is_afternoon_mda = ' + afternoon + ',date_start_mda = "' + start + '",date_end_mda = "' + end + '",is_data_in_sync_mda = 0 WHERE id_mda = ' + cid + '';
    InsertNew2(queryString); /// MISLEADING -- Refactor to be called update rather than insert.

    //    });

}


function multideleteMedia(mid) {
    ///////refactor duplicate method - deleteMedia ////////
    //    var queryString = ' DELETE FROM media_mda WHERE id_mda = '+cid+''
    var queryString = 'UPDATE media_mda SET is_data_deleted_mda = 1, is_data_in_sync_mda = 0 WHERE id_mda = ' + mid + '';
    InsertNew2(queryString);


}


function deleteMedia(cid) {
    ///////refactor duplicate method - multideleteMedia ////////

    var queryString = '  UPDATE media_mda SET is_data_deleted_mda = 1,is_data_in_sync_mda = 0 WHERE id_mda = ' + cid + '';
    InsertNew2(queryString);

}
/// Multipurpose query executor -- if this was renamed it would make more sense.
function InsertNew2(query) {
    pool.getConnection(function(err, connection) {
        connection.query(query, function(err, results, fields) {


            if (err) {
                console.log(err);
            }
            else {
                console.log(results)
            }

        });
        connection.end();
    });
}

//Function to relate accounts to channel groups where
///   act => AccountID and clg => ChannelGroup
function insertIntoAct2CLG(act, clg) {
    pool.getConnection(function(err, connection) {
        connection.query('insert into acttoclg_atc SET ?', {
            idact_atc: act,
            idclg_atc: clg
        }, function(err, result) {
            if (err) {
                console.log(err);
            }
        });
        connection.end();
    });
}
/// This method is to secure the app if the user is not authenticated.  ITs basic.
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}
/// Authenicates a user against known users.
function authenticate(username, password, callback) {

    pool.getConnection(function(err, connection) {
        connection.query('SELECT users_usr.user_name_usr AS username, users_usr.password_usr AS `password`, users_usr.idact_usr AS acctID,  users_usr.id_usr AS userID,  acttoclg_atc.idclg_atc AS cnlgrp,  channel_folders_cnf.id_cnf AS folderID FROM users_usr INNER JOIN acttoclg_atc ON users_usr.idact_usr = acttoclg_atc.idact_atc INNER JOIN channel_folders_cnf ON users_usr.id_usr = channel_folders_cnf.idusr_cnf', function(error, rows, fields) {

            if (error) {
                console.log(error)

            }
            connection.end();
        });
    });

    /// Basically if the user exists return it else return a null
    for (var i = 0; i < users.length; i++) {

        if (users[i]['username'] == username) {
            user = users[i];
            console.log(user);
        }
    }

    if (!user) {
        callback(null);
        return;
    }


    if (user.password == password) {
        callback(user);
        return;
    }
    callback(null);

};
// Function to associate a newly created channel to a folder.
function InsertChannelToFolder(channelID, folderID) {
    var connection = db.createConnection({
        host: host_name,
        user: user_name,
        password: db_pwd,
        database: db_schema,
        stringifyObjects: true
    });
    connection.connect();
    connection.query('INSERT into channel_to_folder_ctf SET ?', {
        idcnf_ctf: folderID,
        idcnl_ctf: channelID
    }, function(err, result) {
        console.log(err);
        console.log(result);
    });


}
// Function to insert Media Tags into the database...
function InsertMediaTags(mediaID, tags)
{
    var tags = tags.split(',');
    pool.getConnection(function(err, connection){
        var length = tags.length,
            element = null;
        var tagid = null;
        for (var i = 0; i < length; i++) {
            element = tags[i];

            connection.query('INSERT into tags_tag SET ?', {
                name_tag: element
            }, function(err, result) {
                console.log(err);
                console.log(result);
                tagid = result.insertId;
                addTagToMedia(mediaID, tagid)
                console.log(tagid);
            });


        }
        connection.end();
    });
}
function addTagToMedia(mediaID, tag) {
    pool.getConnection(function(err, connection) {
        connection.query('INSERT into tagtomda_tgm SET ?', {

            idtag_tgm: tag,
            idmda_tgm: mediaID
        }, function(err, result) {
            console.log(err);
            console.log(result);
        });
    });
}
// Function to insert Channel Tags into the database....
function InsertChannelTags(channel, tags) {
    var tags = tags.split(',');
    pool.getConnection(function(err, connection) {
        var length = tags.length,
            element = null;
        var tagid = null;
        for (var i = 0; i < length; i++) {
            element = tags[i];

            connection.query('INSERT into tags_tag SET ?', {
                name_tag: element
            }, function(err, result) {
                console.log(err);
                console.log(result);
                tagid = result.insertId;
                addTagToChannel(channel, tagid)
                console.log(tagid);
            });


        }
        connection.end();
    });
}
/// Associate the tag to the channel (called from InsertChannelTags)
function addTagToChannel(channelid, tag) {
    pool.getConnection(function(err, connection) {
        connection.query('INSERT into tagtocnl_tgc SET ?', {

            idtag_tgc: tag,
            idcnl_tgc: channelid
        }, function(err, result) {
            console.log(err);
            console.log(result);
        });
    });
}
///Does what it says.  Adds the channel to the Database.
function addChannelToDB(queryn) {
    console.log('addChannelToDB:' + queryn);
    var connection = db.createConnection({
        host: host_name,
        user: user_name,
        password: db_pwd,
        database: db_schema,
        stringifyObjects: true
    });
    connection.connect();
    var q = connection.query(queryn, function(err, result) {


        if (err) {
            console.log(err);
        }
        var channelID = result.insertId;

        connection.end();
        return channelID;
    });

}







/// S3 Movements.  Uses the S3 Module in node_modules
function s3CopyObject(key, copySource) {
    var errorMessage;
    var options = {
        SourceBucket: 'subdc',
        ObjectName: key,
        BucketName: 'subdc',
        SourceObject: copySource
    };
    s3.CopyObject(options, function(err) {
        if (err) {
            console.log('ER' + JSON.stringify(err));
            EmailError(err);
            errorMessage += err;
        }
        
    });
}

////////////////////////Deprecated..... Remove once safe
app.put('/media/:mediaID', function(req, res) {
    console.log(req.body.media_mda);
    res.end();
});

/// Add a Row -- Again this is duplication
function InsertRow(query) {
    pool.getConnection(function(err, connection) {
        connection.query(query, function(err, results, fields) {

            if (err) {
                console.log(err);
            }
        });
        connection.end();
    });
}
/// Update a Row -- Again this is duplication
function UpdateRow(query) {


    pool.getConnection(function(err, connection) {

        connection.query(query, function(err, results, fields) {

            if (err) {
                console.log(err);

            }

        });
        connection.end();
    });

}

/// This is the middle attribute within the routes part at the top of this page.
///  It adds a check for security that the user is logged in....
function requiresLogin(req, res, next) {

    if (req.session.user) {
        next();
    }
    else {
        res.redirect('/login');
    }

}
/////does work to alert cto@sub.tv when a new user registers.
function sendEmail(to) {
    var SendGrid = require('sendgrid').SendGrid;
    var sendgrid = new SendGrid('cto@sub.tv', 'pr0sper');
    sendgrid.send({
        to: to,
        from: 'noreply@subdc.com',
        cc: 'cto@sub.tv',
        subject: 'Welcome to SubDC',
        text: 'Hello! Thank you for joining SubDC.  Your account is now all ready to go.'
    }, function(success, message) {
        if (!success) {
            console.log(message);
        }
    });
}
//////////////Updates the users in memory.
function update_users() {

    connection.query('SELECT users_usr.user_name_usr AS username, users_usr.password_usr AS `password`, users_usr.idact_usr AS acctID, users_usr.id_usr AS userID, acttoclg_atc.idclg_atc AS cnlgrp FROM users_usr INNER JOIN acttoclg_atc ON users_usr.idact_usr = acttoclg_atc.idact_atc', function(error, rows, fields) {


        if (error) {
            console.log(error)
        }

        users = rows;
        connection.end();
    });
}

///not working
function EmailError(message) {
    var SendGrid = require('sendgrid').SendGrid;
    var sendgrid = new SendGrid('cto@sub.tv', 'pr0sper');
    sendgrid.send({
        to: "cto@sub.tv",
        from: 'noreply@subdc.com',
        cc: 'cto@sub.tv',
        subject: 'Error',
        text: message
    }, function(success, message) {
        if (!success) {
            console.log(message);
        }
    });
}
////////////////////////////////////////
/////// ACTUALLY RUN THE APP////////////
////////////////////////////////////////
var port = process.env.PORT || 3000;
app.listen(port);
console.log('Listening on port 3000');